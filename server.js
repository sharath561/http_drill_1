const http = require("http");
const fs = require("fs");
const { v4 } = require("uuid");

const server = http.createServer((req, res) => {
  try {
    if (req.url == "/" && req.method == "GET") {
      res.writeHead(200, { "content-Type": "text/html" });

      fs.readFile("./index.html", "utf-8", (error, data) => {
        if (error) {
          res.write("Error Occured");
        } else {
          res.write(data);
        }
        res.end();
      });
    } else if (req.url == "/json" && req.method == "GET") {
      res.writeHead(200, { "content-Type": "text/json" });

      fs.readFile("./jsonData.json", "utf-8", (error, data) => {
        if (error) {
          res.write("Error");
        } else {
          res.write(data);
        }
        res.end();
      });
    } else if (req.url == "/uuid" && req.method == "GET") {
      res.writeHead(200, { "content-Type": "text/plain" });
      uuidData = JSON.stringify({ uuid: v4() });
      res.end(uuidData);
    } else if (req.method == "GET") {
      let urlData = req.url.split("/");
      let statusCode = urlData[2];
      let format = urlData[1];

      if (format == "status" && req.method == "GET") {
        //console.log("here 2");
        res.writeHead(statusCode, { "content-Type": "text/html" });
        res.write(`<h1>Return a Response with ${statusCode} status code <h1>`);
        res.end();
      } else if (format == "delay" && req.method == "GET") {
        res.writeHead(200, { "content-Type": "text/html" });
        setTimeout(() => {
          res.write(`<h1>I am Waiting for ${statusCode} seconds</h1>`);
          res.end();
        }, parseInt(statusCode * 1000));
      } else {
        res.end("<h1>Invalid request</h1>");
      }
    } else {
      res.end("Invalid request");
    }
  } catch (error) {
    console.log(error);
  }
});

server.listen(8000, () => {
  console.log("server runnig at <http://localhost:8000/>");
});
